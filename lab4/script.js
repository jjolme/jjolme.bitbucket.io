function ej1() {
	var x = parseFloat(prompt('Inserta un numero: '));

	var outputs = "";
	

	for (var i = 0; i <= x; i++) {
		outputs += "<tr>";
			outputs += "<td>" + i     + "</td>";
			outputs += "<td>" + i*i   + "</td>";
			outputs += "<td>" + i*i*i + "</td>";
		outputs += "</tr>";
	}
	

	var html = "<table>" +
	 				"<thead>" +
	 					"<tr>" +
	 						"<th>i</th>"   +
	 						"<th>x^2</th>" +
	 						"<th>x^3</th>" +
	 					"</tr>" +
	 				"</thead>" +
	 				"<tbody>"  +
	 					outputs +
	 				"</tbody>"  +
 				"</table>";

 	document.getElementById("ej1-res").innerHTML = html;
 	//document.write(html);
}

function ej2() {
	var x = prompt("Inserta la posible suma de 2 numeros aleatorios entre [0 y 10) ");
	var n1 = Math.floor(Math.random() * 10),
		n2 = Math.floor(Math.random() * 10);

	if (x == (n1 + n2))
		alert("Felicidades!" + x + " si era la suma. Los numeros eran " + n1 + " + " + n2 + " = " + (n1+ n2));
	else
		alert("Mal! " + x + " no era la suma. Los numeros eran " + n1 + " + " + n2 + " = " + (n1+ n2));
}

function contador(nums) {
	var negativos = 0, 
		zeros = 0,
		positivos = 0;

	for (var n of nums) {
		if (n == 0)
			zeros++;
		else if (n < 0)
			negativos++;
		else
			positivos++;
	}

	return {
		zeros: zeros,
		negativos: negativos,
		positivos: positivos
	};
}

function ej3() {
	var values = document.getElementById('ej3-input').value.split(',')
															.map(parseFloat);
	console.log(values);														
	var res = contador(values);

	var resEl = document.getElementById('ej3-res');
	resEl.innerHTML = "Negativos: " + res.negativos + "<br>" +
					  "Positivos: " + res.positivos + "<br>" +
					  "Cero: " + res.zeros;
}

function promedios(matrix) {
	var proms = [];
	for (var arr of matrix) {
		var total = 0;
		for (var el of arr)
			total += el;

		proms.push(total / arr.length);
	}

	return proms;
}

var ej4Inputs = [];

function addNumberArrayInput() {
	var inputContainer = document.querySelector('.number-array-container');
	var inputEl        = document.getElementById('ej4-input');
	var numbers = inputEl.value.split(',')
								.map(parseFloat);

	// Save Input
	ej4Inputs.push(numbers);

	// Create New Input Entry
	var newInputContainer = document.createElement('div');

	var parentContainer = document.querySelector('#ej4 div');
	var addBtnEl = document.getElementById('ej4-add');
	parentContainer.insertBefore(newInputContainer, addBtnEl)
	newInputContainer.innerHTML = ""+inputContainer.innerHTML;
	newInputContainer.classList="number-array-container";

	// Display Input
	var inLabel = "[";
	for (var n of numbers) {
		inLabel += n + ',';
	}
	inLabel += ']';
	inputContainer.innerHTML = inLabel;
	inputContainer.classList = "";
}

function ej4() {
	var proms = promedios(ej4Inputs);
	var resultEl = document.getElementById('ej4-res')
	var output = "<ul>";

	for (var p of proms) {
		output += "<li>" + p + "</li>";
	}

	output += "</ul>"
	resultEl.innerHTML = output;
}

function inverso(numero) {
	var result = '';
	var nStr = '' + numero;
	
	for (var i = nStr.length-1; i >= 0; i--)  {
		result += nStr.charAt(i);
	}

	return result;
}

function ej5() {
	var input = parseFloat(document.getElementById('ej5-in').value);
	document.getElementById('ej5-res').innerHTML = "" + inverso(input);
}

function esPrimo(n) {
	for (var i = 2; i < Math.floor(n/2); i++) {
		if (n % i == 0)
			return false;
	}

	return true;
}

function ej6() {
	var num = parseFloat(document.getElementById('ej6-in').value);
	var resEl = document.getElementById('ej6-res');

	if (esPrimo(num)) {
		resEl.innerHTML = num + " es primo!";
	} else {
		resEl.innerHTML = num + " no es primo!";
	}

}
